package cleanTest;

import activities.whenDo.CreateTaskScreen;
import activities.whenDo.DeleteTaskScreen;
import activities.whenDo.MainScreen;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import session.Session;

import java.net.MalformedURLException;

public class WhenDoTest {
    private MainScreen mainScreen = new MainScreen();
    private CreateTaskScreen createTaskScreen = new CreateTaskScreen();
    private DeleteTaskScreen deleteTaskScreen = new DeleteTaskScreen();

    @AfterEach
    public void close() throws MalformedURLException {
        Session.getInstance().closeSession();
    }

    @Test
    public void verifyCreateTask() throws MalformedURLException {
        String title = "TEST";
        mainScreen.addTaskButton.click();
        createTaskScreen.titleTextBox.setValue(title);
        createTaskScreen.nameTaskLabel.setValue("Este es un test");
        createTaskScreen.saveButton.click();

        Assertions.assertEquals(title, mainScreen.nameTaskLabel.getText(), "Los Valores no existe");
    }

    @Test
    public void verifyEditTask() throws MalformedURLException {
        String title = "TEST";
        mainScreen.addTaskButton.click();
        createTaskScreen.titleTextBox.setValue(title);
        createTaskScreen.nameTaskLabel.setValue("Este es un test");
        createTaskScreen.saveButton.click();

        String newTitle = "Edit";
        mainScreen.nameTaskLabel.click();
        createTaskScreen.titleTextBox.clearAndSetValue(newTitle);
        createTaskScreen.saveButton.click();

        Assertions.assertEquals(newTitle, mainScreen.nameTaskLabel.getText(), "Los Valores no existe");
    }

    @Test
    public void verifyDeleteTask() throws MalformedURLException {
        String title = "TEST";
        mainScreen.addTaskButton.click();
        deleteTaskScreen.titleTextBox.setValue(title);
        deleteTaskScreen.nameTaskLabel.setValue("Este es un test");
        deleteTaskScreen.saveButton.click();

        mainScreen.nameTaskLabel.click();
        deleteTaskScreen.deleteButton.click();

        deleteTaskScreen.deleteButtonModal.click();

        Assertions.assertEquals("No se agregaron tareas", mainScreen.noNotesLabel.getText(), "Los Valores no existe");
    }
}
